<?php

namespace App\Model;

use Core\App;
use Core\Kernel\AbstractModel;

class ProductsModel extends AbstractModel
{
    protected static $table = 'products';

    protected $id;
    protected $titre;
    protected $reference;
    protected $description;

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return mixed
     */
    public function getTitre()
    {
        return $this->titre;
    }

    /**
     * @return mixed
     */
    public function getReference()
    {
        return $this->reference;
    }

    /**
     * @return mixed
     */
    public function getDescription()
    {
        return $this->description;
    }

    public static function getAllByAlphebeticalOrder($order = 'ASC')
    {
        return App::getDatabase()->query("SELECT * FROM " . self::$table . " ORDER BY titre " . $order, get_called_class());
    }

}