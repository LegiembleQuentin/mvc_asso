<?php

namespace App\Model;

use Core\App;
use Core\Kernel\AbstractModel;

class AbonnesModel extends AbstractModel
{
    protected static $table = 'abonnes';


    protected $id;
    protected $nom;
    protected $prenom;
    protected $email;
    protected $age;
    protected $created_at;

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return mixed
     */
    public function getNom()
    {
        return $this->nom;
    }

    /**
     * @return mixed
     */
    public function getPrenom()
    {
        return $this->prenom;
    }

    /**
     * @return mixed
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * @return mixed
     */
    public function getAge()
    {
        return $this->age;
    }

    /**
     * @return mixed
     */
    public function getCreatedAt()
    {
        return $this->created_at;
    }

    public static function getAllByAlphebeticalOrder($order = 'ASC')
    {
        return App::getDatabase()->query("SELECT * FROM " . self::$table . " ORDER BY nom " . $order, get_called_class());
    }

    public static function getAbonneByEmail($email)
    {
        return App::getDatabase()->prepare("SELECT * FROM " . self::getTable() . " WHERE email = ?",[$email],get_called_class(),true);
    }

    public static function insert($post) : void
    {
        App::getDatabase()->prepareInsert("INSERT INTO " . self::$table . " (nom, prenom, email, age, created_at) VALUES (?,?,?,?,NOW())", array($post['name'], $post['firstname'], $post['email'], $post['age']));
    }

    public static function update($post, $id) : void
    {
        App::getDatabase()->prepareInsert("UPDATE " . self::$table . " SET nom = ?, prenom = ?, email = ?, age = ? 
         WHERE id = ?",
            array($post['name'], $post['firstname'], $post['email'], $post['age'], $id)
        );
    }
}