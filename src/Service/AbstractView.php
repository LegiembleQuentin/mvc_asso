<?php

namespace App\Service;

abstract class AbstractView
{
    public function formatDate($date) {
        $timestamp = strtotime($date);
        $formatted_date = date("d/m/Y", $timestamp);
        return $formatted_date;
    }
}