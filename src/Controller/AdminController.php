<?php

namespace App\Controller;

use Core\Kernel\AbstractController;

/**
 *
 */
class AdminController extends AbstractController
{
    public function index()
    {
        $this->render('app.admin.index',[

        ], 'admin');
    }

}