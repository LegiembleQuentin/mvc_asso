<?php

namespace App\Controller;

use App\Model\AbonnesModel;
use App\Service\Form;
use App\Service\Validation;
use Core\Kernel\AbstractController;

/**
 *
 */
class AbonnesController extends AbstractController {

    public function listing()
    {
        $abonnes = AbonnesModel::getAllByAlphebeticalOrder();

        $this->render('app.admin.abonnes.listing',[
            'abonnes' => $abonnes,
        ], 'admin');
    }

    public function add(){
        $errors = [];

        if(!empty($_POST['submitted'])){
            $post = $this->cleanXss($_POST);

            $validation = new Validation();
            $errors = $this->validate($validation,$post);

            if($validation->IsValid($errors)){
                AbonnesModel::insert($post);
                $this->dump($post);

                $this->redirect('abonnes');
            }
        }
        $form = new Form($errors);

        $this->render('app.admin.abonnes.add-abonne',[
            'form'=>$form,
        ], 'admin');

    }

    public function edit($id)
    {
        $abonne =  $this->getAbonneOr404($id);
        $errors = [];

        if(!empty($_POST['submitted'])) {
            $post = $this->cleanXss($_POST);


            $validation = new Validation();
            $errors = $this->validate($validation,$post,false);

            if ($validation->IsValid($errors)) {
                AbonnesModel::update($post, $id);

                $this->addFlash('success', 'L\'utilisateur a bien été modifié');
                $this->redirect('abonnes');
            }
        }

        $form = new Form($errors);

        $this->render('app.admin.abonnes.edit-abonne',[
            'id' => $id,
            'abonne' => $abonne,
            'form'=>$form,
        ], 'admin');
    }

    public function delete($id){
        $this->getAbonneOr404($id);

        AbonnesModel::delete($id);
        $this->addFlash('success', 'L\'abonné a bien été supprimé');
        $this->redirect('abonnes');
    }

    private function getAbonneOr404($id){
        $abonne = AbonnesModel::findById($id);
        if(empty($abonne)){
            $this->abort404();
        }
        return $abonne;
    }

    private function validate($v,$post, $insert = true)
    {
        $errors = [];
        $errors['name'] = $v->textValid($post['name'], 'nom',2, 255);
        $errors['firstname'] = $v->textValid($post['firstname'], 'prénom',2, 255);
        if (!empty($post['age']) && (!is_numeric($post['age']) || strlen($post['age']) > 3)) {
            $errors['age'] = 'Merci de renseigner un age valide';
        }

        $errors['email'] = $v->emailValid($post['email'], 'email');
        if ($insert){
            if (empty($errors['email'])){
                $testuser = AbonnesModel::getAbonneByEmail($post['email']);
                if (!empty($testuser)){
                    $errors['email'] = 'Cet email correspond déjà à un abonné existant';
                }
            }
        }

        return $errors;
    }

}