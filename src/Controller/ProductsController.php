<?php

namespace App\Controller;

use App\Model\ProductsModel;
use App\Service\Form;
use App\Service\Validation;
use Core\Kernel\AbstractController;

/**
 *
 */
class ProductsController extends AbstractController {
    public function listing()
    {
        $products = ProductsModel::getAllByAlphebeticalOrder();

        $this->render('app.admin.products.listing',[
            'products' => $products,
        ], 'admin');
    }
}