<h1>Nos produits</h1>

<a href="<?= $view->path('ajouter-produits'); ?>">
    <p>Ajouter un produit</p>
</a>

<table>
    <thead>
    <tr>
        <th>Nom</th>
        <th>Reference</th>
        <th>Description</th>
    </tr>
    </thead>
    <tbody>
    <?php foreach($products as $product){ ?>
        <tr>
            <td><?= $product->titre ?></td>
            <td><?= $product->reference ?></td>
            <td><?= $product->description ?></td>
            <td><a href="<?= $view->path('modifier-abonne/'.$product->id) ?>">Editer</a></td>
            <td><a href="<?= $view->path('delete-abonne/'.$product->id) ?>">Supprimer</a></td>
            <td><a href="">Voir</a></td>
        </tr>
    <?php } ?>
    </tbody>
</table>