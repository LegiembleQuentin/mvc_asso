<form action="" method="post">

    <?php
    echo $form->label('name');
    echo $form->input('name', 'text', $abonne->nom ?? '');
    echo $form->error('name');

    echo $form->label('firstname');
    echo $form->input('firstname', 'text', $abonne->prenom ?? '');
    echo $form->error('firstname');

    echo $form->label('email');
    echo $form->input('email', 'text', $abonne->email ?? '');
    echo $form->error('email');

    echo $form->label('age');
    echo $form->input('age', 'number', $abonne->age ?? '');
    echo $form->error('age');

    echo $form->submit('submitted', $submitValue);
    ?>

</form>