<h1>Abonnés</h1>

<a href="<?= $view->path('ajouter-abonne'); ?>">
    <p>Ajouter un abonné</p>
</a>

<table>
    <thead>
    <tr>
        <th>Nom</th>
        <th>Prenom</th>
        <th>Email</th>
        <th>Age</th>
        <th>Incription</th>
    </tr>
    </thead>
    <tbody>
    <?php foreach($abonnes as $abonne){ ?>
        <tr>
            <td><?= strtoupper($abonne->nom) ?></td>
            <td><?= ucfirst($abonne->prenom) ?></td>
            <td><?= $abonne->email ?></td>
            <?php if ($abonne->age){ ?>
                <td><?= $abonne->age ?> ans</td>
            <?php }else { ?>
                <td>--</td>
            <?php }?>
            <td><?= $view->formatDate($abonne->createdAt) ?></td>
            <td><a href="<?= $view->path('modifier-abonne/'.$abonne->id) ?>">Editer</a></td>
            <td><a href="<?= $view->path('delete-abonne/'.$abonne->id) ?>">Supprimer</a></td>
            <td><a href="">Voir plus</a></td>
        </tr>
    <?php } ?>
    </tbody>
</table>