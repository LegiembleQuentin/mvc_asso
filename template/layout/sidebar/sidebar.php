<div id="sidebar">
    <div class="sidebar_content">
        <a class="home_link" href="<?= $view->path(''); ?>"><img src="<?= $view->asset('/img/logo_association.png') ?>" alt=""></a>
        <p class="stats"><a href="">Statistiques ></a></p>

        <div class="container">
            <ul>
                <li><a href="<?= $view->path('abonnes'); ?>">Abonnés ></a></li>
                <li><a href="<?= $view->path('produits') ?>">Produits ></a></li>
            </ul>
        </div>

        <div class="emprunts">
            <p>Emprunts</p>
            <div class="container">
                <ul>
                    <li><a href="">En cours ></a></li>
                    <li><a href="">Historique ></a></li>
                </ul>
            </div>
        </div>
    </div>
</div>